/**
 * @author Vinícius Egidio (vegidio@gmail.com)
 * Sep 1st 2014
 * v1.3
 */

package com.hryun.imdb;

/**
 * Created by vegidio on 9/1/14.
 */
public final class SearchType
{
    public static final String ALL        = "all";
    public static final String TITLE      = "tt";
    public static final String TV_EPISODE = "ep";
    public static final String NAME       = "nm";
    public static final String COMPANY    = "co";
    public static final String KEYWORD    = "kw";
    public static final String CHARACTER  = "ch";
    public static final String QUOTE      = "ch";
}
